from source.abstract.book import Book as AbstractBook
from source.interfaces.model import Model


class Book(AbstractBook, Model):
    @property
    def short(self):
        return f'{self.name}. {self.author} ({self.year})'