from source.abstract.video import Video as AbstractVideo
from source.interfaces.model import Model


class Video(AbstractVideo, Model):
    @property
    def short(self):
        return f'{self.name}. {self.author} ({self.duration})'