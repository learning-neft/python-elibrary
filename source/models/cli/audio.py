from source.abstract.audio import Audio as AbstractAudio
from source.interfaces.model import Model


class Audio(AbstractAudio, Model):
    @property
    def short(self):
        return f'{self.name} ({self.duration} sec)'