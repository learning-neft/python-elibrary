from source.abstract.link import Link as AbstractLink
from source.interfaces.model import Model


class Link(AbstractLink, Model):
    @property
    def short(self):
        return f'{self.name} / {self.url})'