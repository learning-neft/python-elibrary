class Model:
    @property
    def short(self):
        data = [getattr('_' + key) for key in self.fields.keys()]
        return ', '.join(data)

    @property
    def full(self):
        data = []
        for key, info in self.fields.items():
            data.append(': '.join([
                info.get('title'), 
                str(getattr(self, '_' + key))
            ]))
        return '\n'.join(data)