class Media:
    def __init__(self, **kwargs):
        for key, data in self.fields.items():
            default = data.get("default", None)
            setattr(self, '_' + key, default)
        for key, value in kwargs.items():
            if hasattr(self, '_' + key):
                setattr(self, key, value)

    @property
    def type(self):
        return self.__class__.__name__.lower()
        
    @property
    def data(self):
        response = {
            "type": self.type
        }
        for key in self.fields.keys():
            if hasattr(self, '_' + key):
                response[key] = getattr(self, '_' + key)
        return response  

    @property
    def fields(self):
        return {}
    