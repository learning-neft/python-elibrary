from source.abstract import *
from source.interfaces.media import Media

class Library:
    def __init__(self, classes=[]) -> None:
        self._types = {}
        if not len(classes):
            classes = [Book, Link, Video, Audio]
        for cls in filter(lambda c: issubclass(c, Media), classes):
            self._types[cls.__name__.lower()] = cls

    @property
    def types(self):
        return list(self._types.keys())
    
    def get_class_by_type(self, type):
        return self._types.get(type.strip().lower(), None)

    def all(self):
        pass

    def get_one_by_id(self, _id):
        pass

    def save(self, media, _id=None):
        pass

    def delete(self, _id):
        pass

    def find(self, query):
        pass