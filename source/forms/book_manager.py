from datetime import datetime
from source.abstract.book import Book

class BookManager:
    def run(self):
        name = ''
        author = ''
        year = 0
        while not len(name.strip()):
            name = input('Enter name of book: ')
        while not len(author.strip()):
            author = input('Enter author of book: ')
        current_year = datetime.now().year
        while True:
            try:
                year = int(input('Enter public year of book: ').strip())
                if 1700 < year <= current_year:
                    break
                raise
            except:
                print('Book has been public year in between 1700-' + str(current_year))               
        return Book(name=name, author=author, year=year)
        
            
        