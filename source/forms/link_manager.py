from source.abstract.link import Link

class LinkManager:
    def run(self):
        name = ''
        url = ''
        while not len(name.strip()):
            name = input('Enter name of link: ')
        while not len(url.strip()):
            url = input('Enter url of link: ')
        return Link(name=name, url=url)
        
            
        