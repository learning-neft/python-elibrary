from source.abstract.video import Video

class VideoManager:
    def run(self):
        name = ''
        author = ''
        duration = 0
        while not len(name.strip()):
            name = input('Enter name of video: ')
        while not len(author.strip()):
            author = input('Enter author of video: ')
        while True:
            try:
                duration = int(input('Enter duration of video: ').strip())
                break
            except:
                pass               
        return Video(name=name, author=author, duration=duration)
        
            
        