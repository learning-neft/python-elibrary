from source.interfaces.library import Library as AbstractLibrary
from os.path import exists

class Library(AbstractLibrary):
    def __init__(self, config = {}):
        super().__init__(classes=config.get('classes', []))
        self.storage = {}
        self.file = config.get('file', './library.csv')
        if exists(self.file):
            f = open(self.file)
            lines = f.read().split('\n')
            f.close()
            for ix, line in enumerate(lines):
                line = ';'.join(map(str.strip, line.split(';')))
                lines[ix] = line
            lines = list(filter(lambda line: any(line.split(';')), lines))
            if len(lines) < 2:
                return
            _id = 1
            keys = lines[0].split(';')
            for line in lines[1:]:
                item = {}
                for ix, value in enumerate(line.split(';')):
                    if ix >= len(keys):
                        continue
                    item[keys[ix]] = value
                type = item.get('type')
                if not type:
                    continue
                obj = self.get_class_by_type(type)
                if obj is None:
                    continue
                self.storage[_id] = obj(**item)
                _id += 1


    def __save(self):
        prepare = []
        keys = ['type']
        for item in self.storage.values():
            for key in item.fields.keys():
                if key not in keys:
                    keys.append(key)
        prepare.append(keys)
        for item in self.storage.values():
            line = []
            for key in keys:
                if hasattr(item, key):
                    line.append(str(getattr(item, key)))
                else:
                    line.append('')
            prepare.append(line)
        try:
            f = open(self.file, 'w')
            f.write('\n'.join([';'.join(item) for item in prepare]))
            f.close()
        except:
            pass

    def all(self):
        return list(self.storage.items())
    
    def get_one_by_id(self, _id):
        return self.storage.get(_id, None)

    def save(self, media, _id=None):
        if _id is not None and self.get_one_by_id(_id) is None:
            return
        if _id is None:
            _id = 1
            if len(self.storage.keys()):
                _id = max(self.storage.keys()) + 1
        self.storage[_id] = media
        self.__save()

    def delete(self, _id):
        if self.get_one_by_id(_id) is None:
            return
        del self.storage[_id]
        self.__save()
    
    def find(self, query):
        def _filter(media):
            return any(
                filter(
                    lambda value: query.lower() in str(value).lower(), 
                    media.data.values()
                )
            )
        return filter(lambda item: _filter(item[1]), self.all())



            
    