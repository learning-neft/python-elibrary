from source.interfaces.library import Library as AbstractLibrary
from os.path import exists
import json

class Library(AbstractLibrary):
    def __init__(self, config = {}):
        super().__init__(classes=config.get('classes', []))
        self.storage = {}
        self.file = config.get('file', './library.json')
        if exists(self.file):
            f = open(self.file)
            data = json.loads(f.read())
            f.close()
            _id = 1
            for item in data:
                type = item.get('type')
                if type is None:
                    continue
                obj = self.get_class_by_type(type)
                if obj is None:
                    continue
                self.storage[_id] = obj(**item)
                _id += 1


    def __save(self):
        prepare = []
        for item in self.storage.values():
            info = item.data
            prepare.append(info)
        try:
            f = open(self.file, 'w')
            f.write(json.dumps(prepare, indent=4))
            f.close()
        except:
            pass

    def all(self):
        return list(self.storage.items())
    
    def get_one_by_id(self, _id):
        return self.storage.get(_id, None)

    def save(self, media, _id=None):
        if _id is not None and self.get_one_by_id(_id) is None:
            return
        if _id is None:
            _id = 1
            if len(self.storage.keys()):
                _id = max(self.storage.keys()) + 1
        self.storage[_id] = media
        self.__save()

    def delete(self, _id):
        if self.get_one_by_id(_id) is None:
            return
        del self.storage[_id]
        self.__save()
    
    def find(self, query):
        def _filter(media):
            return any(
                filter(
                    lambda value: query.lower() in str(value).lower(), 
                    media.data.values()
                )
            )
        return filter(lambda item: _filter(item[1]), self.all())



            
    