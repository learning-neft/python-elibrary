from source.interfaces.media import Media
from datetime import datetime


class Book(Media):
    @property
    def fields(self):
        return {
            "name": {
                "title": "Name",
                "default": ""
            },
            "author": {
                "title": "Author of book",
                "default": ""
            },
            "year": {
                "title": "Year of publish",
                "default": None
            }
        }
    
    @property
    def name(self):
        return self._name

    @property
    def year(self):
        return self._year

    @property
    def author(self):
        return self._author

    @name.setter
    def name(self, value):
        if not len(value.strip()):
            raise ValueError('Name has not been empty')
        self._name = value
    
    @author.setter
    def author(self, value):
        if not len(value.strip()):
            raise ValueError('Author has not been empty')
        self._author = value

    @year.setter
    def year(self, value):
        try:
            value = int(value)
        except:
            raise ValueError('Year is not number')
        MIN = 1900
        MAX = datetime.now().year + 1
        if MIN <= value <= MAX:
            self._year = value
            return
        raise ValueError(f'Year has been between of [{MIN}, {MAX}]')
