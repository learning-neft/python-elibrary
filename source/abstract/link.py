from source.interfaces.media import Media
import validators

class Link(Media):
    @property
    def fields(self):
        return {
            "name": {
                "title": "Name",
                "default": ""
            },
            "url": {
                "title": "Url of source",
                "default": ""
            }
        }
    
    @property
    def name(self):
        return self._name

    @property
    def url(self):
        return self._url
    
    @name.setter
    def name(self, value):
        if not len(value.strip()):
            raise ValueError('Name has not been empty')
        self._name = value

    @url.setter
    def url(self, value):
        if not len(value.strip()):
            raise ValueError('Url has not been empty')
        if not validators.url(value):
            raise ValueError('Is not url')
        self._url = value
