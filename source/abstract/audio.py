from source.interfaces.media import Media


class Audio(Media):
    @property
    def fields(self):
        return {
            "name": {
                "title": "Name",
                "default": ""
            },
            "duration": {
                "title": "Duration of audio, sec",
                "default": 0
            }
        }
    
    @property
    def name(self):
        return self._name

    @property
    def duration(self):
        return self._duration

    @name.setter
    def name(self, value):
        if not len(value.strip()):
            raise ValueError('Name has not been empty')
        self._name = value

    @duration.setter
    def duration(self, value):
        try:
            value = int(value)
        except:
            raise ValueError('Duration is not number')
        self._duration = value


