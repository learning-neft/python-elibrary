from source.interfaces.media import Media


class Video(Media):
    @property
    def fields(self):
        return {
            "name": {
                "title": "Name",
                "default": ""
            },
            "author": {
                "title": "Author of video",
                "default": ""
            },
            "duration": {
                "title": "Duration of video, sec",
                "default": 0
            }
        }
    
    @property
    def name(self):
        return self._name

    @property
    def duration(self):
        return self._duration

    @property
    def author(self):
        return self._author

    @name.setter
    def name(self, value):
        if not len(value.strip()):
            raise ValueError('Name has not been empty')
        self._name = value

    @author.setter
    def author(self, value):
        if not len(value.strip()):
            raise ValueError('Author has not been empty')
        self._author = value

    @duration.setter
    def duration(self, value):
        try:
            value = int(value)
        except:
            raise ValueError('Duration is not number')
        self._duration = value



